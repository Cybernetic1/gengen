# -*- coding: utf-8 -*-

import os
from random import randint, random

# Make table, one for each possible state of the game
states1 = []
V1 = []
totalStates1 = 0

board = [0, 0, 0, 0, 0, 0, 0, 0, 0]

states2 = []
V2 = []
totalStates2 = 0

board2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]

tiles = [0, 1, -1]

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# FUNCTIONS
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def initBoard():
	for i in range(0, 9):
		board[i] = 0


def switchPlayer(player):
	if player == 1:
		return -1
	else:
		return 1


def printBoard(_board):
	size = len(_board)
	for index in range(0, size):
		if _board[index] == 1:
			print('X', end=' ')
		elif _board[index] == -1:
			print('O', end=' ')
		else:
			print('_', end=' ')
		if 0 == ((index + 1) % 3):
			print()


def hasWinner(_board):
	for player in range(1, 3):
		tile = tiles[player]

		# check horizontal
		for j in range(0, 3):
			i = j * 3
			if (_board[i]     == tile) and \
			   (_board[i + 1] == tile) and \
			   (_board[i + 2] == tile):
				return player

				# check vertical
		for i in range(0, 3):
			if (_board[i]     == tile) and \
			   (_board[i + 3] == tile) and \
			   (_board[i + 6] == tile):
				return player

				# check backward diagonal
		if (_board[0] == tile) and \
		   (_board[4] == tile) and \
		   (_board[8] == tile):
			return player

			# check forward diagonal
		if (_board[6] == tile) and \
		   (_board[4] == tile) and \
		   (_board[2] == tile):
			return player

	# check for draw
	for i in range(0, 9):
		# 0 estimated probability of winning
		if _board[i] == 0:
			return 0

			# -1 is for draw match
	return -1


def updateBoard(_board, player, index):
	if _board[index] == 0:
		_board[index] = player
		return True

	return False


def getListOfBlankTiles():
	blanks = []
	for i in range(0, 9):
		if board[i] == 0:
			blanks.append(i)
	return blanks


# Does not change the board
# maxIndex = index of the board-config in the list of states, which has the max V value
# the list of states is NOT sorted, the max index is found via searching
def greedyMove(states, V, player):
	maxVal = 0
	maxIndex = 0

	nextMoves = getListOfBlankTiles()
	boardIndex = nextMoves.pop()
	board[boardIndex] = player		# try the 1st move
	maxIndex = states.index(board)	# index of the board-config in the list of states
	maxVal = V[maxIndex]			# value of the state
	board[boardIndex] = 0			# undo the move

	for i in nextMoves:
		board[i] = player
		idx = states.index(board)
		if V[idx] > maxVal:
			boardIndex = i
			maxIndex = idx
			maxVal = V[idx]
		board[i] = 0				# undo the move

	return boardIndex, maxIndex


# State-Value Function V(s)
# V(s) = V(s) + alpha [ V(s') - V(s) ]
# s  = current state
# s' = next state
# alpha = learning rate
def BellmanUpdate(sPrime, s, alpha, V):
	V[s] = V[s] + alpha * (V[sPrime] - V[s])


def createStates(filename, states, totalStates):
	fp = open(filename, "w")
	for i in range(0, totalStates):
		state_string = ':'.join([chr(x + ord('0')) for x in states[i]])
		win = hasWinner(states[i])
		if win == 1:			# player 1 win
			value_string = "1.0"
		elif win == 2:			# player -1 win
			value_string = "0.0"
		elif win == -1:			# draw
			value_string = "0.5"
		else:
			value_string = str(random());
		fp.write("%s %s\n" % (state_string, value_string))
	fp.close()


def saveStatesToFile(filename, states, V, totalStates):
	fp = open(filename, "w")
	for i in range(0, totalStates):
		state_string = ':'.join([chr(x + ord('0')) for x in states[i]])
		value_string = str(V[i])
		fp.write("%s %s\n" % (state_string, value_string))
	fp.close()


def loadStatesFromFile(fp, states, V):
	total = 0
	while True:
		line = fp.readline()
		if line == "":
			break
		first_split = line.split(' ')
		state = [ord(x[0]) - ord('0') for x in first_split[0].split(':')]
		value = float(first_split[1])
		states.append(state)
		# print state
		V.append(value)
		total = total + 1
	# print "Loading...."
	# raw_input
	return total

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# PROGRAM STARTS HERE
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# step-size parameter (rate of learning)
alpha = 0.01

# exploration rate
exploreRate = 0.1

filename1 = "ttt1.dat"
filename2 = "ttt2.dat"
# Create table of all possible TicTacToe states

# Build states for RL player 1
fp = open(filename1, "r")
print("Loading states from file...")
totalStates1 = loadStatesFromFile(fp, states1, V1)
fp.close()

# createStates("ttt0.dat", states1, totalStates1)
# print "Created new value map: ttt0.dat"
# exit(0);

# Build states for RL player 2
fp = open(filename2, "r")
print("Loading states from file...")
totalStates2 = loadStatesFromFile(fp, states2, V2)
fp.close()

print("Player 1 Total States: %d" % (totalStates1))
print("Player 1 Total States: %d" % (totalStates2))
# input("Press <Enter> to continue!")

# board config --> logical facts representation
def encode_state():
	for i, c in enumerate(board):
		if c == 1:
			add_proposition(0x08, i, 1)
		elif c == -1:
			add_proposition(0x08, i, 2)

def play_game():
	playTimes = 1000  # number of plays during Learning Phase (default 100000)

	numPlayer1Won = 0  # number of games won Player 1
	numPlayer2Won = 0  # number of games won Player 2
	numDraws = 0  # number of draws
	player = 1  # starts with Player 1 during Learning Phase

	while (1):  # For N trials
		# ensure board is all zeroes
		initBoard()

		# player 1 = 1 and player 2 = -1

		# Game phase:
		# randomly select who goes first
		player = randint(1, 2)
		if player == 2:
			player = -1

		# Prints board
		print("Game %d \r" % (playTimes), end='\n')
		# printBoard(board)
		# print

		prevIndex1 = 0  # previous state index
		maxIndex1 = 0  # index with maximum state value
		firstMove1 = True  # flag for first play of a game episode

		prevIndex2 = 0  # previous state index
		maxIndex2 = 0   # index with maximum state value
		firstMove2 = True # flag for first play of a game episode

		while (True):  # For each game

			nextMoves = getListOfBlankTiles()
			countNextMoves = len(nextMoves)
			exploring = False

			# print "Player %d's move:" %(player)

			ex = randint(1, 100) / 100.0

			# player 2 -- don't change the code
			if player == -1:
				if ex <= exploreRate:
					userPlay = nextMoves[randint(0, countNextMoves - 1)]
					exploring = True
					# print "exploring"
				else:
					userPlay, maxIndex2 = greedyMove(states2, V2, player)
					# print "greedy"
					if not firstMove2:
						# print "V2(s) = %f changed to" %(V2[prevIndex2]),
						BellmanUpdate(maxIndex2, prevIndex2, alpha, V2)
						# print "V2(s) = %f" %(V2[prevIndex2])
					prevIndex2 = maxIndex2

					# player 2 has made one move
					firstMove2 = False

			# player 1 (Computer)
			# How to make a move?
			# * set up "facts" = state of the board
			# * execute forward chaining
			# * if output is active, make move otherwise loop
			else:
				if ex <= exploreRate:
					userPlay = nextMoves[randint(0, countNextMoves - 1)]
					exploring = True
					# print "exploring"
				else:
					encode_state()
					# run inferences
					while True:
						output = forward_chain()
						# if output == action:
						if output[0] == 0x0a:
							userPlay = output[1]
							break
						append(states1, output)

					# userPlay, maxIndex1 = greedyMove(states1, V1, player)
					# print "greedy"
					if not firstMove1:
						# print "V1(s) = %f changed to" %(V1[prevIndex1]),
						BellmanUpdate(maxIndex1, prevIndex1, alpha, V1)
						# print "V1(s) = %f" %(V1[prevIndex1])
					prevIndex1 = maxIndex1

					# player 1 has made one move
					firstMove1 = False

			# update board from user play (might be unnecessary)
			if updateBoard(board, player, userPlay) == True:
				if exploring:
					if player == 1:
						prevIndex1 = states1.index(board)
					else:
						prevIndex2 = states2.index(board)
			else:
				print("见鬼！\n")
				exit(0)

			# printBoard(board)

			won = hasWinner(board)
			if won > 0:
				if 1 == player:
					# player 1 wins (player 1 just made the last move)
					numPlayer1Won = numPlayer1Won + 1
					# This is a losing state whose value should be zero because it could never
					# get updated;  Therefore the following Bellman update works:
					maxIndex2 = states2.index(board)
					# print "V2(s) = %f changed to" %(V2[prevIndex2]),
					BellmanUpdate(maxIndex2, prevIndex2, alpha, V2)
					# print "V2(s) = %f" %(V2[prevIndex2])

				else:
					# player -1 wins
					numPlayer2Won = numPlayer2Won + 1
					maxIndex1 = states1.index(board)
					# print "V1(s) = %f changed to" %(V1[prevIndex1]),
					BellmanUpdate(maxIndex1, prevIndex1, alpha, V1)
					# print "V1(s) = %f" %(V1[prevIndex1])

				# print "Player %d has won!" %(player)
				# print
				break

			if won == -1:
				numDraws = numDraws + 1
				#maxIndex = states.index(board)
				#print "V(s) = %f changed to" %(V[prevIndex]),
				#BellmanUpdate(maxIndex, prevIndex)
				#print "V(s) = %f" %(V[prevIndex])
				# print "It's a draw!"
				# print
				break

			player = switchPlayer(player)
			# print

		if playTimes:
			playTimes = playTimes - 1
		# elif input("Play Again[y]: ") != "y":
		else:
			break

	saveStatesToFile(filename1, states1, V1, totalStates1)
	saveStatesToFile(filename2, states2, V2, totalStates2)

	print()
	print("-----")
	print("Game Stats: ")
	print("Player 1  Wins  : %d" % (numPlayer1Won))
	print("Player -1 Wins  : %d" % (numPlayer2Won))
	print("          Draws : %d" % (numDraws))
