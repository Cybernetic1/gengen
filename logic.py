# -*- coding: utf-8 -*-

# Single-step forward chaining
# ============================
# Algorithm:
# * from working_memory, find rules that match
# * points are propositions, each time we get a match increment
# * return matched rule (randomly with probability distribution)
def single_step_forward_chain():
	score = [0] * pop_size

	for i, rule in enumerate(population):
		for prop in working_mem:
			# evaluate rule, some rules will exceed threshold
			if evaluate_formula(rule[0], prop):		# rule[0] = pre-condition
				score[i] += 1

	# for all rules that are satisfied, randomly pick a winner
	# use Efraimidis-Spirakis (2006) algorithm as suggested by Paulo Marques F. on StackExchange
	# Zen (https://stats.stackexchange.com/users/9394/zen), Choosing elements of an array based on given probabilities for each element, URL (version: 2015-05-02): https://stats.stackexchange.com/q/134756
	score2 = []
	for i, rule in enumerate(population):
		if score[i] >= rule[2]:
			u = random.uniform(0.0, 1.0)
			p = rule[3]
			append(score2, (i, u**(1/p)))


# input: prop = binary string = list of Booleans
# 		 formula = Boolean formula (assume in prefix format)
# algorithm: substitute prop's bits into formula's variables, then evaluate Boolean expression
# I guess formulas do not need to contain Boolean constants (True or False)
def evaluate_formula(node, prop):
	if isinstance(node, list):
		arg1 = evaluate_formula(node[1], prop)
		arg2 = evaluate_formula(node[2], prop)
		return node[0](*[arg1, arg2])
	elif:					# isinstance(node, int):
		return prop[node]

# This is old code:
def evaluate_rule(formula, prop):
	sym = formula[0]		# head symbol in current sub-formula
	if sym == 0:			# end of Boolean formula
		return (0.0, None)
	elif sym == 1:			# Boolean ¬
		val, tail = evaluate_rule(formula[1:], prop)
		return (1.0 - val, tail)
	elif sym == 2:			# Boolean ∧
		val1, tail1 = evaluate_rule(formula[1:], prop)
		val2, tail2 = evaluate_rule(tail1, prop)
		return (val1 * val2, tail2)
	elif sym == 3:			# Boolean ∨
		val1, tail1 = evaluate_rule(formula[1:], prop)
		val2, tail2 = evaluate_rule(tail1, prop)
		return (val1 + val2, tail2)
	else:					# Boolean variables
		pos = sym - 4		# bit 'position' of the variable
		val = prop[pos]		# value of variable = truth value of bit inside proposition
		return (val, formula[1:])
